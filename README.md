# dockerC

## About
Tiny docker container around docker-clean for running inside our stack. It removes all the unused docker container images on the host it is run on.

## Usage

run with:

```
docker run -v /var/run/docker.sock:/var/run/docker.sock -t -i ryandick/dockerc
```

Or copy the `Dockerfile` and build it your self
