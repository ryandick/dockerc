FROM alpine:edge
RUN echo "http://dl-4.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
RUN apk update
RUN apk upgrade
RUN apk add --update iojs git python build-base
RUN rm -rf /var/cache/apk/*

ENV NPM_CONFIG_LOGLEVEL info
ENV DOCKER_HOST unix:///var/run/docker.sock

RUN npm install -g docker-clean

CMD docker-clean -H $DOCKER_HOST
